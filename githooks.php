<?php
/**
 * add crontab -- * * * * * php /home/web/tools/adminer/githooks.php
 */
$folder = __DIR__ . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;

$name = '';

// Repo authorized
$listRepoAuth = [
    // git@gitlab.com:devmr/htmlhexoblog.git
    'htmlhexoblog' => [
        'folder' => '/home/web/miaryrabs/',
        'branch' => 'master',
        'reponame' => 'gitlab',
    ],
    // git@gitlab.com:devmr/reactbuilt_covid19mg.git
    'reactbuilt_covid19mg' => [
        'folder' => '/home/web/covid19mg/react/',
        'branch' => 'master',
        'reponame' => 'origin',
    ],
    // git@gitlab.com:devmr/vuejsbuilt_covid19mg.git
    'vuejsbuilt_covid19mg' => [
        'folder' => '/home/web/covid19mg/vue/',
        'branch' => 'master',
        'reponame' => 'origin',
    ],
    // git@gitlab.com:devmr/angularbuilt_covid19mg.git
    'angularbuilt_covid19mg' => [
        'folder' => '/home/web/covid19mg/angular/',
        'branch' => 'master',
        'reponame' => 'origin',
    ],
    
    // git@gitlab.com:devmr/schoolmg_kits_symfony.git
    'schoolmg_kits_symfony' => [
        'folder' => '/home/web/schoolmg.app/skeleton_src/symfony',
        'branch' => 'master',
        'reponame' => 'origin',
    ],
];

// Go to miaryrabs; pull and remove files inside tmp
$cmd = 'cd %s; git pull %s %s; rm -f %s';

// if calling from php cli
if (isset($argv)) {


    // file exists inside tmp folder
    $files = glob($folder . "*");
    if (!empty($files)) {
        foreach ($files as $file) {
            // Content
            $content = file_get_contents($file);

            // Parse json content
            $json = json_decode($content,true);
            if (isset($json['project']['name']) &&
                in_array($json['project']['name'], array_keys($listRepoAuth))) {

                $name = $json['project']['name'];
                $folder = $listRepoAuth[$name]['folder'];
                $reponame = $listRepoAuth[$name]['reponame'];
                $branch = $listRepoAuth[$name]['branch'];

                $cmdShell = sprintf(
                    $cmd,
                    $folder,
                    $reponame,
                    $branch,
                    $file
                );
//                echo $cmdShell;
                shell_exec($cmdShell);
            }
        }

        exit;
    }

}

// if calling from webhooks
$sInput = 'php://input';
$json_str = file_get_contents($sInput);
if ($json_str != '') {

    $json = json_decode($json_str, true);

//    print_r($json)
    // if json >> save inside tmp folder
    if (is_array($json)) {
        // save request_payload inside txt
        $f = $folder . uniqid() . '.txt';
//        print_r($f);
        file_put_contents($f, $json_str);

        // mail
        $to = 'rabehasy@gmail.com';
        $s = 'Webhooks ' . $name;
        $m = print_r($json, 1);
        $h = 'From:webmaster@madatsara.com';
        mail($to, $s, $m, $h);
        exit;
    }

}

// if calling from GET
echo "Hello";
